package com.diplomado.claro.proyecto.application.port.in.employee;

public interface DeleteEmployeeCommand {
    void execute(Long id);
}
