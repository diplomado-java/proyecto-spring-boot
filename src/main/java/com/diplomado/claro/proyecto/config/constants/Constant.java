package com.diplomado.claro.proyecto.config.constants;

import lombok.Value;

@Value
public class Constant {
    public static final String MSG_EMPLOYEE_CREATED = "Employee created successfully";
    public static final String MSG_EMPLOYEE_UPDATED = "Employee updated successfully";
    public static final String MSG_EMPLOYEE_DELETED = "Employee deleted successfully";
}
